const _setTechnology = tech => ({type: "SET_TECHNOLOGY", tech});

export default _setTechnology;