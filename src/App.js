import React, { Component } from 'react';
import HelloWorld from './HelloWorld';
import ButtonGroup from './ButtonGroup';
import { store } from './store';

class App extends Component {

  render() {
    //can use array in return, but add key prop
    return [
      <HelloWorld key={1} tech={store.getState().tech} />,
      <ButtonGroup key={2} technologies={["React", "Elm", "React-redux"]} />
    ];

    // return (
    //   <div>
    //     <HelloWorld key={1} tech={store.getState().tech} />,
    //     <ButtonGroup key={2} technologies={["React", "Elm", "React-redux"]} />
    //   </div>
    //   );
  }
}

export default App;
